## How to run the project

In the project directory, first download all the packages required by running

### `npm install`

Then start the project by running

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

Use the upload button to upload an input file and click Solve!

![alt text](./Challenge_Demo.png "Demo: ")

To run tests for the word search solver algorithm on the given sample data, simply run

### `npm test`
