import React, { useEffect, useState } from "react";

/*
Displays the wordbank and takes in props from parent component, WordSearchDisplay.
Displays the list of wors that need to be searched, followd by the correct
answers of the word search when 'Solve!' button is clicked.
*/
function WordBank(props) {
  //stores the list of words that need to be searched
  const [words, setWords] = useState([]);
  //stores the answer object that has the start and end positions for each word
  const [answer, setAnswer] = useState(null);

  //Renders when the prop is changed
  useEffect(() => {
    setWords(props.words);
    setAnswer(props.answer);
  }, [props]);

  return (
    <div className="container">
      {words === undefined ? null : (
        <div className="wordBank">
          <div className="wordHeader">
            <div>Word</div>
            {answer === null || answer === undefined ? (
              <div>Bank</div>
            ) : (
              <div>Answers</div>
            )}
          </div>
          {words.map((word, rowId) => {
            return (
              <div className="wordRow" key={rowId}>
                {answer === null || answer === undefined ? (
                  <div key={rowId}>{word}</div>
                ) : (
                  <>
                    <div>{`${word} ${answer[word].startRow}:${answer[word].startCol} ${answer[word].endRow}:${answer[word].endCol}`}</div>
                  </>
                )}
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
}

export default WordBank;
