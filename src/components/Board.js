import React, { useEffect, useState } from "react";

function Board(props) {
  const [board, setBoard] = useState(null);

  useEffect(() => {
    setBoard(props.board);
  }, [props]);

  return (
    <div className="container">
      {board === null ? null : (
        <div className="board">
          {board.map((row, rowId) => {
            return (
              <div className="row" key={rowId}>
                {row.map((alphabet, cellId) => {
                  return (
                    <div className="cell" key={cellId}>
                      {alphabet}
                    </div>
                  );
                })}
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
}

export default Board;
