import React, { useState, useEffect, useRef } from "react";
import Board from "./Board";
import WordBank from "./WordBank";
import solver from "../functions/Solver";

/*
WordSearch allows the user to upload file and
displays the wordsearch board and wordbank by parsing the
uploaded file. It then solves the word search when the 'Solve!' button is clicked.
*/
function WordSearch() {
  //uploaded file
  const [file, setFile] = useState(null);
  //uploaded file name
  const [fileName, setFileName] = useState("");
  //stores the board data with dimensions, alphabets in 2D array, and array of words that need to be searched
  const [info, setInfo] = useState(null);
  //file reference
  const fileRef = useRef(null);
  //stores the word puzzle answer, the start and end positions of words
  const [answer, setAnswer] = useState(null);
  //file reader
  var fileReader = new FileReader();

  //Rerenders the file name when new file is uploaded
  useEffect(() => {
    if (file != null) {
      setFileName(file.name);
    }
  }, [file]);

  //Triggered when the uploaded file changes.
  //Reads the file by triggering handleFileRead() after fileReader has finished loading.
  const handleFileChange = (event) => {
    try {
      setFile(event.target.files[0]);
      fileReader.onloadend = handleFileRead;
      if (event.target.files[0] !== undefined) {
        fileReader.readAsText(event.target.files[0]);
      }
    } catch (e) {
      console.log("error occured while handling file change:", e);
    }
  };

  //Parses the text file and creates 2D array for the board and stores the alphabets into their corresponding positions
  //Stores the list of words that need to be searched
  const parseBoard = (input) => {
    try {
      //parse row and column for the board
      const row = input[0];
      const col = input[2];

      //2D array that represents the board
      let board = [];

      //separate the input text line by line
      const lines = input.split("\n");

      //iterate through the input and parse the
      //alphabets of the board to the board array
      let r = 1;
      while (r <= row) {
        board.push(lines[r].split(" "));
        r++;
      }

      //stores the words that need to be found
      let words = [];

      while (lines[r] && r < lines.length) {
        words.push(lines[r]);
        r++;
      }

      //returns the parsed and processed data
      setInfo({
        row: row,
        col: col,
        board: board,
        words: words,
      });
    } catch (e) {
      console.log("error occurred while parsing file data");
    }
  };

  //Reads the file and parses the board
  //Resets the answer to null every time new file is read
  const handleFileRead = () => {
    parseBoard(fileReader.result);
    if (answer != null) {
      setAnswer(null);
    }
  };

  //Triggered when upload button is clicked
  //The reference is used to mitigate the design issue of file upload button
  const handleUpload = () => {
    fileRef.current.click();
  };

  //Triggered when solve button is clicked
  //Populates the answer state variable from the result of solver function
  const onClickSolve = () => {
    if (info !== null) {
      setAnswer(solver(info.board, info.words, info.row, info.col));
    }
  };

  return (
    <div className="container">
      <div className="upload">
        <button className="buttonUpload" onClick={handleUpload}>
          Upload file
        </button>
        <input
          type="file"
          name="fileUplaod"
          ref={fileRef}
          onChange={handleFileChange}
          accept=".txt"
          style={{ display: "none" }}
        />
        <em style={{ padding: 10 }}>{fileName}</em>
      </div>
      <div>
        {info === null ? (
          <div>
            <em>No file currently selected</em>
          </div>
        ) : (
          <div>
            <div className="wordsearch">
              <Board board={info.board} />
              <WordBank words={info.words} answer={answer} />
            </div>
            <button onClick={onClickSolve} className="buttonSolve">
              Solve!
            </button>
          </div>
        )}
      </div>
    </div>
  );
}

export default WordSearch;
