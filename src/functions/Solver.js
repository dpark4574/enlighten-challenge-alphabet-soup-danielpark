/*
Iterates through the board and checks if the current letter is
a start of any word in the word bank. If it is, then it calls check direction function
to check the rest of the letters. If a word is found, it appends to the answer
*/
function solver(board, words, row, col) {
  //final answer
  let answer = {};
  //keeps track of the found words
  let found = [];

  for (let r = 0; r < row; r++) {
    for (let c = 0; c < col; c++) {
      //keeps track of the current letter being processed in the board
      let curr = board[r][c];
      //check if the current letter is the starting letter for any of the word
      for (let w = 0; w < words.length; w++) {
        //skip the word if already found
        if (!found.includes(words[w])) {
          //removes the space in the word
          let currWord = words[w].replace(/\s/g, "");
          //if the current letter is a starting letter,
          if (curr === currWord[0]) {
            //will have the position of the last letter of the word if the word was found
            //null if not
            let endCoords = checkDirection(board, currWord, r, c);
            if (endCoords != null) {
              answer[words[w]] = {
                startRow: r,
                startCol: c,
                endRow: endCoords.endRow,
                endCol: endCoords.endCol,
              };
              found.push(words[w]);
            }
          }
        }
      }
    }
  }
  return answer;
}

/*
Checks which direction the next letter of the word is and
checks the rest of the letters in that direction to find the word
*/
function checkDirection(board, word, currRow, currCol) {
  //checks all the direction (top-left, top, top-right, left, right, bottom-left, bottom, bottom-right)
  for (let x_dir = -1; x_dir < 2; x_dir++) {
    for (let y_dir = 1; y_dir > -2; y_dir--) {
      //updates the coordinate that checks the next letter in the word
      let checkingX = currRow + x_dir;
      let checkingY = currCol + y_dir;
      if (
        checkingX >= 0 &&
        checkingX < board.length &&
        checkingY >= 0 &&
        checkingY < board[0].length
      ) {
        //no need to check the initial letter
        if (!(checkingX === currRow && checkingY === currCol)) {
          //check if the current letter is the next letter in the word
          if (board[checkingX][checkingY] === word[1]) {
            //found the next character and the direction the rest needs to be checked
            //check the rest of the direction
            return checkDirectionRest(
              board,
              word,
              checkingX + x_dir,
              checkingY + y_dir,
              x_dir,
              y_dir
            );
          }
        }
      }
    }
  }
}

//Called when the first and second letters have been a match
//Checks the rest of the letters in the direction of the word to find the word
function checkDirectionRest(board, word, currRow, currCol, x_dir, y_dir) {
  //flags if letter doesn't match the word
  let found = true;
  //checks the rest of the letters of the word
  for (let i = 2; i < word.length && found; i++) {
    //if it goes out of bound, the word doesn't exist
    if (
      currRow < 0 ||
      currRow >= board.length ||
      currCol < 0 ||
      currCol >= board[0].length
    ) {
      found = false;
    }
    //if letter matches, check the next letter in that direction
    if (board[currRow][currCol] === word[i]) {
      //updates the row with the x-coordinate direction weight
      currRow += x_dir;
      //updates the col with the y-coordinate direction weight
      currCol += y_dir;
    } else {
      found = false;
    }
  }
  return found ? { endRow: currRow - x_dir, endCol: currCol - y_dir } : null;
}

export default solver;
