import solver from "./Solver";

//Tests the wordsearch solver for sample 1
test("sample 1", () => {
  let board = [];
  board.push(["A", "B", "C"]);
  board.push(["D", "E", "F"]);
  board.push(["G", "H", "I"]);

  let words = [];
  words.push("ABC");
  words.push("AEI");

  expect(solver(board, words, 3, 3)).toEqual({
    ABC: {
      startRow: 0,
      startCol: 0,
      endRow: 0,
      endCol: 2,
    },
    AEI: {
      startRow: 0,
      startCol: 0,
      endRow: 2,
      endCol: 2,
    },
  });
});

//Tests the wordsearch solver for sample 2
test("sample 2", () => {
  let board = [];
  board.push(["H", "A", "S", "D", "F"]);
  board.push(["G", "E", "Y", "B", "H"]);
  board.push(["J", "K", "L", "Z", "X"]);
  board.push(["C", "v", "B", "L", "N"]);
  board.push(["G", "O", "O", "D", "O"]);

  let words = [];
  words.push("HELLO");
  words.push("GOOD");
  words.push("BYE");

  expect(solver(board, words, 5, 5)).toEqual({
    HELLO: {
      startRow: 0,
      startCol: 0,
      endRow: 4,
      endCol: 4,
    },
    GOOD: {
      startRow: 4,
      startCol: 0,
      endRow: 4,
      endCol: 3,
    },
    BYE: {
      startRow: 1,
      startCol: 3,
      endRow: 1,
      endCol: 1,
    },
  });
});
