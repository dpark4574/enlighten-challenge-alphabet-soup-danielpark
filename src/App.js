import "./App.css";
import React from "react";
import WordSearch from "./components/WordSearch";

function App() {
  return (
    <div className="App">
      <header className="header">
        <h1>Welcome to Alphabet Soup</h1>
      </header>
      <div className="container">
        <WordSearch />
      </div>
    </div>
  );
}

export default App;
